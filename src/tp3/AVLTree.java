package tp3;

import java.util.LinkedList;
import java.util.Queue;

public class AVLTree<Element extends Comparable<Element>> {
	private Element root;
	private AVLTree<Element> leftChild, rightChild, parent;
	private int height;
	private int posCompletBTree;
	private int range;

	public AVLTree(Element root) {
		this.root = root;
		leftChild = rightChild = parent = null;
		height = 0;
		posCompletBTree = 0;
		range = 0;
	}

	public void insert(Element element) {

		if (element.compareTo(this.root) < 0) {
			if (this.leftChild == null) {
				this.leftChild = new AVLTree<Element>(element);
				this.leftChild.parent = this;
				this.leftChild.root = element;
				System.out.println("insert: " + this.leftChild.root);
			}
			if (this.leftChild != null) {
				this.leftChild.insert(element);
			}
		}

		if (element.compareTo(this.root) > 0) {
			if (this.rightChild == null) {
				this.rightChild = new AVLTree<Element>(element);
				this.rightChild.parent = this;
				this.rightChild.root = element;
				System.out.println("insert: " + this.rightChild.root);
			}
			if (this.rightChild != null) {
				this.rightChild.insert(element);
			}
		}

		this.height = this.max_height_childs() + 1;
		this.equilibrage();
	}

	public void equilibrage() {
		int heightright;
		int heighleft;
		if (this.rightChild == null) {
			heightright = -1;
		} else {
			heightright = this.rightChild.height;
		}
		if (this.leftChild == null) {
			heighleft = -1;
		} else {
			heighleft = this.leftChild.height;
		}
		if ((heighleft - heightright) > 1) {
			this.rotation_droite();
		}
		if ((heightright - heighleft) > 1) {
			this.rotation_gauche();
		}
	}

	public void rotation_droite() {
		int height_leftChild_leftChild;
		if (this.leftChild.leftChild == null)
			height_leftChild_leftChild = -1;
		else
			height_leftChild_leftChild = this.leftChild.leftChild.height;

		if (height_leftChild_leftChild == this.leftChild.height - 1) {
			System.out.println("je suis dans le cas d'une rotation simple droite pour l'arbre de racine: " + this.root);
			this.rotation_simple_droite();

		} else {
			System.out.println("je suis dans le cas d'une rotation double droite pour l'arbre de racine: " + this.root);
			this.rotation_double_droite();
		}
	}
	
	public void rotation_gauche() {
		int height_rightChild_rightChild;
		if (this.rightChild.rightChild == null)
			height_rightChild_rightChild = -1;
		else
			height_rightChild_rightChild = this.rightChild.rightChild.height;

		if (height_rightChild_rightChild == this.rightChild.height - 1) {
			System.out.println("je suis dans le cas d'une rotation simple gauche pour l'arbre de racine: " + this.root);
			this.rotation_simple_gauche();

		} else {
			System.out.println("je suis dans le cas d'une rotation double gauche pour l'arbre de racine: " + this.root);
			this.rotation_double_gauche();
		}
	}
	
	public void rotation_simple_droite() {
		Element thisroot = this.root;
		this.root = this.leftChild.root;
		this.leftChild.root = thisroot;
		AVLTree<Element> filsgauche = this.leftChild;
		AVLTree<Element> filsdroit = this.rightChild;
		AVLTree<Element> filsgauche_gauche = this.leftChild.leftChild;
		AVLTree<Element> filsgauche_droit = this.leftChild.rightChild;

		this.leftChild = filsgauche_gauche;
		this.leftChild.parent = this;
		this.rightChild = filsgauche;
		this.rightChild.leftChild = filsgauche_droit;
		if (this.rightChild.leftChild != null) {
			this.rightChild.leftChild.parent = this.rightChild;
		}
		this.rightChild.rightChild = filsdroit;
		if (this.rightChild.rightChild != null) {
			this.rightChild.rightChild.parent = this.rightChild;
		}

		this.leftChild.height = this.leftChild.max_height_childs() + 1;
		this.rightChild.height = this.rightChild.max_height_childs() + 1;
		this.height = this.max_height_childs() + 1;
	}

	public void rotation_double_droite() {
		Element thisroot = this.root;
		this.root = this.leftChild.rightChild.root;
		this.leftChild.rightChild.root = thisroot;

		AVLTree<Element> filsdroit = this.rightChild;
		AVLTree<Element> filsgauche_droit = this.leftChild.rightChild;
		AVLTree<Element> filsgauche_droit_gauche = this.leftChild.rightChild.leftChild;
		AVLTree<Element> filsgauche_droit_droit = this.leftChild.rightChild.rightChild;

		this.rightChild = filsgauche_droit;
		this.rightChild.parent = this;

		this.leftChild.rightChild = filsgauche_droit_gauche;
		if (this.leftChild.rightChild != null) {
			this.leftChild.rightChild.parent = this.leftChild;
		}

		this.rightChild.leftChild = filsgauche_droit_droit;
		if (this.rightChild.leftChild != null) {
			this.rightChild.leftChild.parent = this.rightChild;
		}

		this.rightChild.rightChild = filsdroit;
		if (this.rightChild.rightChild != null) {
			this.rightChild.rightChild.parent = this.rightChild;
		}

		this.leftChild.height = this.leftChild.max_height_childs() + 1;
		this.rightChild.height = this.rightChild.max_height_childs() + 1;
		this.height = this.max_height_childs() + 1;

	}

	public void rotation_simple_gauche() {
		Element thisroot = this.root;
		this.root = this.rightChild.root;
		this.rightChild.root = thisroot;
		AVLTree<Element> filsdroit = this.rightChild;
		AVLTree<Element> filsgauche = this.leftChild;
		AVLTree<Element> filsdroit_gauche = this.rightChild.leftChild;
		AVLTree<Element> filsdroit_droit = this.rightChild.rightChild;

		this.rightChild = filsdroit_droit;
		this.rightChild.parent = this;
		this.leftChild = filsdroit;
		this.leftChild.leftChild = filsgauche;
		if (this.leftChild.leftChild != null) {
			this.leftChild.leftChild.parent = this.leftChild;
		}
		this.leftChild.rightChild = filsdroit_gauche;
		if (this.leftChild.rightChild != null) {
			this.leftChild.rightChild.parent = this.leftChild;
		}

		this.leftChild.height = this.leftChild.max_height_childs() + 1;
		this.rightChild.height = this.rightChild.max_height_childs() + 1;
		this.height = this.max_height_childs() + 1;
	}

	public void rotation_double_gauche() {
		Element thisroot = this.root;
		this.root = this.rightChild.leftChild.root;
		this.rightChild.leftChild.root = thisroot;

		AVLTree<Element> filsgauche = this.leftChild;
		AVLTree<Element> filsdroit_gauche = this.rightChild.leftChild;
		AVLTree<Element> filsdroit_gauche_gauche = this.rightChild.leftChild.leftChild;
		AVLTree<Element> filsdroit_gauche_droit = this.rightChild.leftChild.rightChild;

		this.leftChild = filsdroit_gauche;
		this.leftChild.parent = this;

		this.leftChild.leftChild = filsgauche;
		if (this.leftChild.leftChild != null) {
			this.leftChild.leftChild.parent = this.leftChild;
		}

		this.leftChild.rightChild = filsdroit_gauche_gauche;
		if (this.leftChild.rightChild != null) {
			this.leftChild.rightChild.parent = this.leftChild;
		}

		this.rightChild.leftChild = filsdroit_gauche_droit;
		if (this.rightChild.leftChild != null) {
			this.rightChild.leftChild.parent = this.rightChild;
		}

		this.leftChild.height = this.leftChild.max_height_childs() + 1;
		this.rightChild.height = this.rightChild.max_height_childs() + 1;
		this.height = this.max_height_childs() + 1;
	}

	public int max_height_childs() {
		int heightLeftChild, heightRightChild;
		if (this.leftChild != null)
			heightLeftChild = this.leftChild.height;
		else
			heightLeftChild = -1;
		if (this.rightChild != null)
			heightRightChild = this.rightChild.height;
		else
			heightRightChild = -1;

		if (heightRightChild > heightLeftChild)
			return heightRightChild;
		else
			return heightLeftChild;
	}

	public void delete(Element element) {
		if (element.compareTo(this.root) == 0) {
			AVLTree<Element> maxleft;
			if (this.leftChild != null) {
				maxleft = this.leftChild.max_of_lef_tree();
			} else {
				maxleft = this;
			}
			Element updateroot = maxleft.root;
			maxleft.parent.update_tree_maxparent(updateroot);
			this.root = updateroot;
			System.out.println("delete: "+element);
		}
		if ((element.compareTo(this.root) > 0) && (this.rightChild != null)) {
			this.rightChild.delete(element);
		}
		if ((element.compareTo(this.root) < 0) && (this.leftChild != null)) {
			this.leftChild.delete(element);
		}
		this.height = this.max_height_childs() + 1;
		this.equilibrage();
		return;
	}

	public AVLTree<Element> max_of_lef_tree() {
		if (this.rightChild == null) {
			return this;
		} else {
			return this.rightChild.max_of_lef_tree();
		}
	}

	public void update_tree_maxparent(Element element) {

		if (this.leftChild != null) {
			if (this.leftChild.root == element)
				this.leftChild = null;
		}
		if (this.rightChild != null) {
			if (this.rightChild.root == element) {
				if (this.rightChild.height == 0) {
					this.rightChild = null;
				} else {
					this.rightChild.root = this.rightChild.leftChild.root;
					this.rightChild.leftChild = null;
					this.rightChild.height--;
				}
			}
			
		}


	}

	public boolean contains(Element element) {
		if (element.compareTo(this.root) == 0) {
			System.out.print(element + " est pr�sent? ");
			return true;
		}
		if ((element.compareTo(this.root) > 0) && (this.rightChild != null)) {
			return this.rightChild.contains(element);
		}
		if ((element.compareTo(this.root) < 0) && (this.leftChild != null)) {
			return this.leftChild.contains(element);
		}
		System.out.print(element + " est pr�sent? ");
		return false;
	}

	public void display_tree() {
		System.out.println();
		System.out.println("--------------Affichage de l'arbre----------------\n");

		Queue<AVLTree<Element>> file = new LinkedList<>();
		file.add(this);

		this.range = 1;
		this.posCompletBTree = 1;

		int height = this.height;
		int tabulationsize = (int) ((Math.pow(2, height + 1) - 1));
		int rangenoeudprecede = 1;
		int lastPosInRange = 0;
		int posInRange = -1;
		int subForRange = 0;

		boolean hasoneinrange = false;

		tabulation_line(tabulationsize);

		while (!file.isEmpty()) {
			AVLTree<Element> noeudcourant = file.poll();

			if (noeudcourant.range > rangenoeudprecede) {
				System.out.println();
				lastPosInRange = -1;
				tabulationsize = (tabulationsize / 2);
				hasoneinrange = false;
			}

			if (lastPosInRange == -1) {
				tabulation_line(tabulationsize);
				lastPosInRange = 0;
			}

			subForRange = (int) Math.pow(2, (noeudcourant.range - 1));
			rangenoeudprecede = noeudcourant.range;
			posInRange = noeudcourant.posCompletBTree - subForRange;
			int nbGap = posInRange - lastPosInRange;

			if (!hasoneinrange) {
				tab_first_in_line(tabulationsize, nbGap);
			}
			if (hasoneinrange) {
				tab_not_first_in_line(tabulationsize, nbGap);
			}

			lastPosInRange = posInRange;

			System.out.print(noeudcourant.root + "" + noeudcourant.height);
			hasoneinrange = true;

			if (noeudcourant.leftChild != null) {
				noeudcourant.leftChild.posCompletBTree = 2 * noeudcourant.posCompletBTree;
				noeudcourant.leftChild.range = noeudcourant.range + 1;
				file.add(noeudcourant.leftChild);
			}

			if (noeudcourant.rightChild != null) {
				noeudcourant.rightChild.posCompletBTree = (2 * noeudcourant.posCompletBTree) + 1;
				noeudcourant.rightChild.range = noeudcourant.range + 1;
				file.add(noeudcourant.rightChild);
			}
		}
		System.out.println("\n");
	}

	
	public void tabulation_line(int tabulationsize) {
		for (int i = 0; i < tabulationsize; i++) {
			System.out.print(" ");
		}
	};

	public void tab_first_in_line(int tabulationsize, int nbGap) {

		int iterator = 2 * nbGap * (tabulationsize + 1);
		for (int i = 0; i < ((iterator)); i++) {
			System.out.print(" ");
		}
	}

	
	private void tab_not_first_in_line(int tabulationsize, int nbGap) {
		if ((nbGap == 0) || (nbGap == 1)) {
			int iterator = 2 * nbGap * tabulationsize;
			for (int i = 0; i < iterator; i++) {
				System.out.print(" ");
			}
		} else {
			int iterator = (2 * nbGap * (tabulationsize + 1)) - 2;
			for (int i = 0; i < ((iterator)); i++) {
				System.out.print(" ");
			}
		}
	}

}
