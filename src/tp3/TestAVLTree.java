package tp3;
public class TestAVLTree {
	public static void main(String[] args) {
		AVLTree<String> avltree = new AVLTree<String>("f");
		AVLTree<String> avltreeRSD = new AVLTree<String>("f");
		AVLTree<String> avltreeRSG = new AVLTree<String>("f");
		AVLTree<String> avltreeRDD = new AVLTree<String>("f");
		AVLTree<String> avltreeRDS = new AVLTree<String>("f");
		
		
//------test_rotation_simple_droite------
		
		avltreeRSD.insert("h");
		avltreeRSD.insert("d");
		avltreeRSD.insert("b");
		avltreeRSD.insert("e");
		avltreeRSD.insert("a");
		avltreeRSD.display_tree();

		
//------test_rotation_simple_gauche------
		
		avltreeRSG.insert("i");
		avltreeRSG.insert("a");
		avltreeRSG.insert("g");
		avltreeRSG.insert("y");
		avltreeRSG.insert("z");
		avltreeRSG.display_tree();
	
		
//------test_rotation_double_droite------	
		
		avltreeRDD.insert("g");
		avltreeRDD.insert("b");
		avltreeRDD.insert("a");
		avltreeRDD.insert("d");
		avltreeRDD.insert("c");
		avltreeRDD.display_tree();
		
		
//------test_rotation_double_gauche------
		
		avltreeRDS.insert("d");
		avltreeRDS.insert("m");
		avltreeRDS.insert("g");
		avltreeRDS.insert("z");
		avltreeRDS.insert("l");
		avltreeRDS.display_tree();
		
		
		
//------Tests_finals/finaux_sur_l'ensemble_alphabet------		
		avltree.insert("k");
		avltree.insert("b");
		avltree.insert("e");			
		avltree.insert("h");
		avltree.insert("i");
		avltree.insert("d");
		avltree.insert("b");
		avltree.insert("c");
		avltree.insert("q");
		avltree.insert("l");				
		avltree.insert("u");
		avltree.insert("w");
		avltree.insert("n");
		avltree.insert("s");		
		avltree.insert("m");	
		avltree.insert("t");		
		avltree.insert("v");
		avltree.insert("w");
		avltree.insert("p");
		avltree.insert("o");
		avltree.insert("s");		
		avltree.insert("n");	
		avltree.insert("m");
		avltree.insert("j");
		avltree.insert("a");
		avltree.insert("g");
		avltree.insert("r");		
		avltree.insert("y");
		avltree.insert("x");
		avltree.insert("z");		
		
		System.out.println(avltree.contains("e"));
		System.out.println(avltree.contains("c"));
		System.out.println(avltree.contains("b"));
		System.out.println(avltree.contains("c"));
		System.out.println(avltree.contains("n"));
		
		avltree.display_tree();
		
		avltree.delete("l");
		System.out.println(avltree.contains("l"));
		avltree.delete("v");
		System.out.println(avltree.contains("v"));
		avltree.delete("x");
		System.out.println(avltree.contains("x"));
		avltree.delete("e");
		System.out.println(avltree.contains("e"));
	
		
		avltree.display_tree();
	}
}
